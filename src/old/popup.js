const machina = require('machina')

var sessionManager = new machina.Fsm({
  namespace: 'session-manager',
  initialState: 'reading-session',
  states: {
    'reading-session': {
      _onEnter() {
        let sessionData = sessionStorage.getItem('email-widget-shown')
        if (sessionData !== null) {
          this.transition('sessionExists')
        }
        else {
          this.transition('sessionDoesNotExists')
        }
      }
    },
    sessionExists() {
      // do nothing
    },
    sessionDoesNotExists: {
      _onEnter() {
        this.handle('createSession')
      },
      createSession() {
        sessionStorage.setItem('email-widget-shown', 'true')
      }
    }
  }
})

var Form = function(selector) {
  this.selector = selector;
  this.form = document.querySelector(this.selector)
  this.setIntroAnimation = function() {
    let form = this.form
    setTimeout(function() {
      if( form.classList ) form.classList.add('show-up')
      else form.classList += ' ' + 'show-up'
    }, 300)
  }
  this.setFocusAnimation = function() {
    this.form.addEventListener('focusin', function(e) {
      this.style.transform = 'translateY(2px)';
    })
    this.form.addEventListener('focusout', function(e) {
      this.style.transform = 'none';
    })
  }
  this.setOutroAnimation = function() {
    let form = this.form
    let dismiss = this.form.querySelector('.dismiss')
    dismiss.addEventListener('click', function() {
      setTimeout(function() {
        if (dismiss) { form.classList.remove('show-up') }
      }, 200)
    })
  }
}

module.exports = {
  sessionManager: sessionManager,
  Form: Form
}