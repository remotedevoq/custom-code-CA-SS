const R = require('ramda')

const functions = {
  getNodeList: selectors => document.querySelectorAll(selectors),
  getStyleAttr: element => element.getAttribute('style'),
  setStyleAttr     : (element, newValue) => { element.setAttribute('style', newValue) },
  stringToInt      : string => parseInt(string, 10),
  getIntString     : R.compose(R.head, R.match(/[-+]?\d*/g)),
  roundNumber      : number => Math.round(number),
  addPx            : x => typeof x == 'string' ? x + 'px' : String(x) + 'px',
  percentageOf     : R.curry( (percentage, value) => Math.abs(value) * (percentage / 100) ),
  increaseByPercent: R.curry( (percentage, value) => R.add( value, functions.percentageOf(percentage, value) ) ),
  decreaseByPercent: R.curry( (percentage, value) => R.subtract( value, functions.percentageOf(percentage, value) ) ),
  cssStringToPropObj: R.pipe(
    R.replace(/\s/g, ''),
    R.split(';'),
    R.dropLast(1),
    R.map(
      R.pipe(
        R.split(':'),
        R.converge( R.objOf, [R.head, R.last])
      )
    ),
    R.mergeAll),
  increasePropBy: (lens, percentage) => {
    return R.over(lens, R.pipe(
      functions.getIntString,
      functions.stringToInt,
      functions.increaseByPercent(percentage),
      functions.roundNumber,
      functions.addPx
    ))
  },
  decreasePropBy: (lens, percentage) => {
    return R.over(lens, R.pipe(
      functions.getIntString,
      functions.stringToInt,
      functions.decreaseByPercent(percentage),
      functions.roundNumber,
      functions.addPx
    ))
  },
  convertToStyleAttr: R.pipe( R.toPairs, R.map( R.join(':')), R.join(';'), x => x + ';')
}

module.exports = functions