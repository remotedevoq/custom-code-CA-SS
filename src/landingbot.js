let umiwebchatContainer = document.createElement('div')
umiwebchatContainer.id = 'umiwebchat-container'

let landBotConfig = document.createElement('script')
landBotConfig.text = "var LandbotLiveConfig = { index: 'https://landbot.io/u/H-22934-97T1IBI6XPF6ZDR7/index.html', accent: '#A2BFC3' };"

let landbotJS = document.createElement('script')
landbotJS.setAttribute('src', 'https://static.helloumi.com/umiwebchat/umiwebchat.js')
landbotJS.setAttribute('defer', 'true')

module.exports = {
  umiwebchatContainer: umiwebchatContainer,
  landBotConfig: landBotConfig,
  landbotJS: landbotJS
}

// <div id="umiwebchat-container"></div><script>var LandbotLiveConfig = { index: 'https://landbot.io/u/H-22934-97T1IBI6XPF6ZDR7/index.html', accent: '#A2BFC3' };</script><script src="https://static.helloumi.com/umiwebchat/umiwebchat.js" defer></script>