import css from './scss/style.scss'
const R      = require('ramda')
const Router = require('./Router.js')

import {
  getNodeList,
  getStyleAttr,
  setStyleAttr,
  stringToInt,
  getIntString,
  roundNumber,
  addPx,
  percentageOf,
  increaseByPercent,
  decreaseByPercent,
  cssStringToPropObj,
  increasePropBy,
  decreasePropBy,
  convertToStyleAttr,
  updateImgStyles
} from './functions.js'

import { umiwebchatContainer, landBotConfig, landbotJS} from './landingbot.js'

function ready(fn) {
  if (document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
    fn()
  }
  else {
    document.addEventListener('DOMContentLoaded', fn)
  }
}


// Lenses
const leftLens   = R.lensProp('left')
const topLens    = R.lensProp('top')
const widthLens  = R.lensProp('width')
const heightLens = R.lensProp('height')

let webchatContainer = document.createElement('div')
    webchatContainer.id = 'webchat-container'
    webchatContainer.appendChild(umiwebchatContainer)
    webchatContainer.appendChild(landBotConfig)
    webchatContainer.appendChild(landbotJS)

ready( () => {
  let pageRouter = new Router(window.location.href)
  pageRouter.startWatching()

  // Data
  const desktopMediaQuery       = window.matchMedia( "(min-width: 1024px )")
  const imageCollectionSelector = '.main-content-inner-wrapper .index-gallery-wrapper .collection-images .image-container img'


  // =================================
  // START OFF RUNNING CODE
  // =================================
  setTimeout( () => {
    if (window.location.pathname == '/') document.body.appendChild(webchatContainer)

    if ( !R.contains(window.location.pathname, ['/shout', '/shout/','/about', '/about/','/press', '/press/']) ) {
      // Check if media query and route match
      if ( desktopMediaQuery.matches ) {

        let indexNavigation = document.querySelector('.collection-nav-desc-wrapper > nav')
        let siteHeader      = document.querySelector('.site-header')
        let mainMenu        = document.querySelector('.site-header .nav-social-wrapper')
        let navItems        = indexNavigation.querySelectorAll('.collection-nav-item-span')

        // Moving the menu
        siteHeader.insertBefore(indexNavigation, mainMenu)

        // Replacing the menu itmes for •'s
        if (navItems.length > 0) R.forEach( item => item.innerText = '•', navItems)

        // Adding the background to the index navigation only in the desktop view.
        if (window.location.pathname !== '/') {
          indexNavigation.classList.add('index-navigation-page-view__visibility')
        }
      }

      else {
        // Adding responsive class
        let indexNavigation = document.querySelector('.collection-nav-desc-wrapper > nav')
        indexNavigation.classList.add('responsive-vertical-menu')

        //  Making the transformations for the images
        // the or was added because the imgList selectors vary dependinding from where page are you coming.
        // If the blog pages is loaded first, these images will have to be reached through the second selector
        let imgList = window.location.pathname == '/' ? getNodeList(imageCollectionSelector) : getNodeList('.index-nav .collection-images.index-nav-images img')
        let imgStyleProps = R.map(R.pipe(getStyleAttr, cssStringToPropObj))(imgList)
        let updatedImgsStyleProps = R.map( R.pipe(
          increasePropBy(widthLens, 18),
          increasePropBy(heightLens, 18),
          decreasePropBy(leftLens, 25)
        ))(imgStyleProps)


        console.log(imgList)
        let updatedStyleString = R.map(convertToStyleAttr)(updatedImgsStyleProps)

        R.pipe(
          R.zip(imgList),
          R.forEach(
            x => { setStyleAttr(x[0], x[1])}
          )
        )(updatedStyleString)


      }
    }
  }, 100)


  // =================================
  // Router watcher
  // =================================
  // Implement changes on route change.
  window.addEventListener('router-update', (event) => {


      document.body.appendChild(webchatContainer)

      // If the current route is not any of these
      if ( !R.contains(window.location.pathname, ['/shout', '/shout/','/about', '/about/','/press', '/press/']) ) {

        if (desktopMediaQuery.matches) {
          let indexNavigation         = document.querySelector('.collection-nav-desc-wrapper > nav')
          let previousIndexNavigation = document.querySelector('.site-header .index-navigation')
          let siteHeader              = document.querySelector('.site-header')
          let mainMenu                = document.querySelector('.site-header .nav-social-wrapper')
          let navItems                = indexNavigation.querySelectorAll('.collection-nav-item-span')

          if (previousIndexNavigation) {
            siteHeader.removeChild(previousIndexNavigation)
          }
          siteHeader.insertBefore(indexNavigation, mainMenu)

          // Replacing the menu itmes for •'s
          if (navItems.length > 0) {
            R.forEach( item => item.innerText = '•', navItems)
          }

          if (window.location.pathname !== '/') {
            // Adding the index-navigation background only in the desktop view
            indexNavigation.classList.add('index-navigation-page-view__visibility')
          }

          if (window.location.pathname == '/') {
            indexNavigation.classList.remove('index-navigation-page-view__visibility')
          }


        }
        else {
          let imgList = getNodeList(imageCollectionSelector)
          // Adding responsive class
          let indexNavigation = document.querySelector('.collection-nav-desc-wrapper > nav')
          indexNavigation.classList.add('responsive-vertical-menu')

          //  Making the transformations for the images
          let imgStyleProps = R.map(R.pipe(getStyleAttr, cssStringToPropObj))(imgList)
          let updatedImgsStyleProps = R.map( R.pipe(
            increasePropBy(widthLens, 18),
            increasePropBy(heightLens, 18),
            decreasePropBy(leftLens, 25)
           ))(imgStyleProps)
          let updatedStyleString = R.map(convertToStyleAttr)(updatedImgsStyleProps)


         R.pipe(
            R.zip(imgList),
            R.forEach(
              x => { setStyleAttr(x[0], x[1])}
            )
          )(updatedStyleString)
        }

      if (window.location.pathname !== '/' ) {
        document.body.removeChild(webchatContainer)
      }

    }

  })

})