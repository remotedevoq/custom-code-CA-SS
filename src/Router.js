const routerUpdated = new Event('router-update')

class Router {
  constructor(url) {
    this.currentURL = url || ''
  }
  get getURL() {
    return this.currentURL
  }
  set updateURL(url) {
    this.currentURL = url
  }
  startWatching() {
    this.interval = setInterval( () => {
      if (this.getURL !== window.location.href) {
        this.updateURL = window.location.href
        window.dispatchEvent(routerUpdated)
      }
    }, 200)
  }
  stopWatching() {
    clearInterval(this.interval)
  }
}

module.exports = Router